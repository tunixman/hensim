module Text.Hensim.Word2Vec where

import Data.Text
import Text.Hensim.Vocabulary (Vocabulary, TrimRule)

data TrainingAlgorithm =
  SG |
  CBOW Bool

type WordHash = Text -> Int

data Word2VecConf = Word2VecConf {
  size :: Integer,
  alpha :: Double,
  window :: Integer,
  min_count :: Integer,
  max_vocab :: Integer,
  sample :: Double,
  seed :: Double,
  min_alpha :: Double,
  training :: TrainingAlgorithm,
  hierarchicalSampling :: Bool,
  negative :: Integer,
  hashfxn :: WordHash,
  iter :: Integer,
  trim_rule :: TrimRule
  }

data Word2Vec = Word2Vec {
  conf :: Word2VecConf,
  index2word :: [Text],
  sentences :: [[Text]]
  }

{-|
    Update skip-gram model by training on a sequence of sentences
    Each sentence is a list of string tokens, which are looked up
    in the model's vocab dictionary. Called internally from `WordToVec.train`
-}
trainBatchSg :: Model -> Sentences -> Alpha -> Work -> Int
trainBatchSg = undefined


{-|
    Update CBOW model by training on a sequence of sentences
    Each sentence is a list of string tokens, which are looked up
    in the model's vocab dictionary. Called internally from `WordToVec.train`

    Neu1 ~ Vector ?
-}
trainBatchCbow :: Model -> Sentences -> Alpha -> Work -> Neu1 -> Int
trainBatchCbow = undefined


{-|
    Obtain likelihood score for a single sentence in a fitted skip-gram
    representation

    The sentence is a list of Vocab objects (or None, when the corresponding
    word is not in the vocabulary). Called internally from `Word2Vec.score`
-}
scoreSentenceSg :: Model -> Sentence -> Work -> Double
scoreSentenceSg = undefined


{-|
    Obtain likelihood score for a single sentence in a fitted CBOW representaion.

    The sentence is a list of Vocab objects (or None, where the corresponding
    word is not in the vocabulary. Called internally from `Word2Vec.score

    Neu1 ~ Vector ?
-}
scoreSentenceCbow :: Model -> Sentence -> Alpha -> Work -> Neu1 -> Double
scoreSentenceCbow = undefined


{-|
    Not sure if the return type is correct here

    neu1e :: Vector?
-}
trainSgPair :: Model -> Word -> ContentIndex -> Alpha -> LearnVectors -> LearnHidden
            -> ContextVectors -> ContextLocks -> Vector --?
trainSgPair = undefined


{-|
    Or this one
-}
trainCbowPair :: Model -> Word -> InputWordIndices -> L1 -> Alpha -> LearnVectors
              -> LearnHidden -> Vector
trainCbowPair = undefined


scoreSgPair :: Model -> Word -> Word2 -> Double
scoreSgPair = undefined


scoreCbowPair :: Model -> Word -> Word2Indices -> L1 -> Double
scoreCbowPair = undefined
